const express = require('express');
const app = express();
const port = 3000;
const bodyParser = require('body-parser')


app.set('views', './views');

app.set('view engine', 'pug');
app.use(express.urlencoded())


const mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test', {useNewUrlParser: true});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
});

 rsvpSchema = new mongoose.Schema({
    name: String,
    email: String,
    attending: Boolean,
    number: Number,
  });

  const rsvp = mongoose.model('rsvp', rsvpSchema);

  app.get('/', function (req, res){
      res.render('form')
  })
  app.post('/response', function (req, res){
      console.log(req.body)
       newRsvp = new rsvp({ 
           name:req.body.name,
           email:req.body.email,
           attending: req.body.attending,
           Number: req.body.friends
     });
newRsvp.save(function (err, newRsvp){
    console.log(err)
})
      res.render('response')
  })
  app.get('/guestlist', (req, res) => {
      rsvp.find({},(err, responses) => {
        console.log(responses)
        let attendArr = []
        let notAttendArr = []
        for(item of responses){
            if(item.attending === 'yes') {
                attendArr.push(item)
        } else {
            notAttendArr.push(item)
        }
      }
      res.render('guestlist',{
          attending: attendArr,
          notAttending: notAttendArr
      })
    })
})

  
  app.listen(port, ()=> console.log(`Expample app listening on port $({port}!`));

